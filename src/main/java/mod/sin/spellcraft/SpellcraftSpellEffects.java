package mod.sin.spellcraft;

import com.wurmonline.server.spells.EnchantUtil;

import java.util.ArrayList;
import java.util.logging.Logger;

public class SpellcraftSpellEffects {
	protected static final Logger logger = Logger.getLogger(SpellcraftSpellEffects.class.getName());

	protected static ArrayList<ArrayList<Byte>> enchantGroups = new ArrayList<>();

	public static void addEnchantGroup(String[] split){
	    ArrayList<Byte> newGroup = new ArrayList<>();
	    for(String ench : split){
	        newGroup.add(Byte.valueOf(ench));
        }
        enchantGroups.add(newGroup);
    }
	public static void onServerStarted(){
		if (SpellcraftMod.improvedEnchantGrouping) {
			EnchantUtil.enchantGroups = enchantGroups;
		}
	}
}
